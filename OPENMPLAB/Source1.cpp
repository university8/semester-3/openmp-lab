#include <omp.h>
#include <iostream>
#include <cmath>
#include <functional>
const double pi = 3.14159265358979323846;
double simpson(double a, double b, int n, std::function<double(double a)> f) {
	double h = (b - a) / n;
	double sum_odds = 0.0;
#pragma omp parallel for default(none) shared(a,f,n,h) reduction(+:sum_odds)
	for (int i = 1; i < n; i += 2)
	{
		sum_odds += f(a + i * h);
	}
	double sum_evens = 0.0;
#pragma omp parallel for default(none) shared(a,f,n,h) reduction(+:sum_evens)
	for (int i = 2; i < n; i += 2)
	{
		sum_evens += f(a + i * h);
	}
	return (f(a) + f(b) + 2 * sum_evens + 4 * sum_odds) * h / 3;
}
double f1(double x) {
	return 2*x-2;
}
double f2(double x) {
	return sin(x-1);
}
double defaulValue(double a,double b) {
	double res1 = -(pow((a - 1), 2));
	double res2 = 1 - (cos(1 - b));
	return res1 + res2;
}
int main() {
	double a;
	double b = 1 + pi / 2;
	int n;
	std::cout << "N: ";
	std::cin >> n;
	std::cout << "a: ";
	std::cin >> a;
	if (a >= 1 || n<0) {
		return 0;
	}
	if (n % 2 != 0)
		n += 1;
	double res1 = simpson(a, 1, n/2, f1);
	double res2 = simpson(1, b, n/2, f2);
	std::cout <<"Integral: "<< res1 + res2 << std::endl;
	std::cout <<"Default value: "<<defaulValue(a,b) << std::endl;
}