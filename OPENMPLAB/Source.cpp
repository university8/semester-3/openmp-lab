#include <omp.h>
#include <iostream>
#include <cmath>
#include <climits>

const double pi = 3.14159265358979323846;

double f(double x) {
    return sin(x) - x;
}

int main() {
    double a = -pi / 2;
    double b = pi / 2;
    double n;
    std::cout << "N: ";
    std::cin >> n;
    if (n < 0) {
        return 0;
    }
    double h = (b - a) / n;
    double der;
    double xi;
    int i = 0;
    double max = INT_MIN;
#pragma omp parallel default(none) shared(n, max, h, a, b) private(i, xi, der)
    {
        double local_max = INT_MIN;
#pragma omp for
        for (i = 0; i < (int)n - 1; i++) {
            xi = a + h * (i);
            der = (f(xi + h) - 2 * f(xi) + f(xi - h)) / (h * h);
            if (der > local_max)
                local_max = der;
        }
#pragma omp critical
        {
            if (local_max > max) {
                max = local_max;
            }
        }
    }
    std::cout << "Max: " << max << std::endl;
    std::cout << "Default max: " << 1 << std::endl;
}
